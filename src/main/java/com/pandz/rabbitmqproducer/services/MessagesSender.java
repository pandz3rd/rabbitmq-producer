package com.pandz.rabbitmqproducer.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pandz.rabbitmqproducer.constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MessagesSender {
  private static final Logger log = LoggerFactory.getLogger(MessagesSender.class);

  @Autowired
  private RabbitTemplate rabbitTemplate;

  public void send(String name) {
    log.info("Sending messages to RabbitMq....");
    rabbitTemplate.convertAndSend(Constants.HELLO_JANGKRIK, String.format("Hello, my name is %s", name));
  }

  public void sendEmail(Map<String, String> map) {
    log.info("Sending messages to RabbitMq....");
    try {
      ObjectMapper mapper = new ObjectMapper();
      rabbitTemplate.convertAndSend(Constants.EMAIL, mapper.writeValueAsString(map));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }
}
